﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using G2V.Models;
using System.Data.Entity;

namespace G2V.Controllers
{
    public class ProduitController : Controller
    {
        // GET: Produit
        private g2vEntities db = new g2vEntities();
        public ActionResult Produit()
        {
            return View(db.voiture.ToList());
        }
        public ActionResult Create()
        {
            return View();
        }
        // POST: Produit/Create
        [HttpPost]
        public ActionResult Create(voiture voiture)
        {
            try
            {
                // TODO: Add insert logic here
                db.voiture.Add(voiture);
                db.SaveChanges();

                return RedirectToAction("Produit");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Read(String num_voiture)
        {
            return View(db.voiture.Where(x => x.num_voiture == num_voiture).FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Update(String num_voiture)
        {
            return View(db.voiture.Where(x => x.num_voiture == num_voiture).FirstOrDefault());

        }
        // POST: Client/Edit/5
        [HttpPost]
        public ActionResult Update(string num_voiture, voiture voiture)
        {
            try
            {
                // TODO: Add update logic here
                db.Entry(voiture).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Produit");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Delete(String num_voiture)
        {
            return View(db.voiture.Where(x => x.num_voiture == num_voiture).FirstOrDefault());

        }
        [HttpPost]
        public ActionResult Delete(string num_voiture, voiture voiture)
        {
            try
            {
                // TODO: Add delete logic here
                voiture Vdelete = db.voiture.Where(x => x.num_voiture == num_voiture).FirstOrDefault();
                db.voiture.Remove(Vdelete);
                db.SaveChanges();


                return RedirectToAction("Produit");
            }
            catch
            {
                return View();
            }
        }
    }
}