﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using G2V.Models;
using System.Data.Entity;

namespace G2V.Controllers
{
    public class ClientController : Controller
    {

        private g2vEntities db = new g2vEntities();
        public ActionResult Client()
        {
            return View(db.client.ToList());
        }

        // GET: Client/Create
        public ActionResult Create()
        {
            return View();
        }
        // POST: Client/Create
        [HttpPost]
        public ActionResult Create(client client)
        {
            try
            {
                // TODO: Add insert logic here
                db.client.Add(client);
                db.SaveChanges();
                return RedirectToAction("Client");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Read(String num_client)
        {
            return View(db.client.Where(x => x.num_client == num_client).FirstOrDefault());
        }

        [HttpGet]
        public ActionResult Update(String num_client)
        {
            return View(db.client.Where(x => x.num_client == num_client).FirstOrDefault());
            
        }
        // POST: Client/Edit/5
        [HttpPost]
        public ActionResult Update(string num_client, client client)
        {
            try
            {
                // TODO: Add update logic here
                db.Entry(client).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Client");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Delete(String num_client)
        {
            return View(db.client.Where(x => x.num_client == num_client).FirstOrDefault());

        }
        [HttpPost]
        public ActionResult Delete(string num_client, client client)
        {
            try
            {
                // TODO: Add delete logic here
                client clientdelete = db.client.Where(x => x.num_client == num_client).FirstOrDefault();
                db.client.Remove(clientdelete);
                db.SaveChanges();


                return RedirectToAction("Client");
            }
            catch
            {
                return View();
            }
        }
    }
}