﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using G2V.Models;
using System.Data.Entity;

namespace G2V.Controllers
{
    public class VenteController : Controller
    {
        private g2vEntities db = new g2vEntities();
        
        // GET: Vente
        public ActionResult Vente()
        {
            return View(db.ventes.ToList());
        }
        public ActionResult Create()
        {
            return View();
        }
        // POST: Produit/Create
        [HttpPost]
        public ActionResult Create(ventes vente)
        {
            
            if (!(IsFindClient(vente.num_client) && IsFindVoiture(vente.num_voiture))) {
                return View();
            }
            else { 
            int? QTE; int? QT;
            int? qte = vente.qte;
            var label = db.voiture.Where(x => x.num_voiture ==vente.num_voiture).ToList();
            foreach (var item in label)
            {
                QTE = item.stock;
                QT = QTE - qte;
                UpdateQTE(QT,vente.num_voiture);
            };

            try
            {
                // TODO: Add insert logic here
                db.ventes.Add(vente);
                db.SaveChanges();

                return RedirectToAction("Vente");
            }
            catch
            {
                return View();
            }
            }
        }

        public bool IsFindClient(string num)
        {
             var numC = db.client.ToList();
            foreach (var i in numC)
            {
                if (i.num_client.Equals(num)) { return true; };
            };

            ViewBag.notFindID = "Le numéro du Client que vous avez entré n'existe pas";
            return false;
        }
        public bool IsFindVoiture(string num)
        {
             var numV = db.voiture.ToList();
            foreach (var i in numV)
            {
                if (i.num_voiture.Equals(num)) { return true; };
            };
            ViewBag.notFindID = "Le numéro de la Voiture que vous avez entré n'existe pas";
            return false;
        }

        [HttpGet]
        public ActionResult Read(int Id)
        {
            return View(db.ventes.Where(x => x.Id == Id).FirstOrDefault());
        }

        [HttpGet]
        public ActionResult Update(int Id)
        {
            return View(db.ventes.Where(x => x.Id == Id).FirstOrDefault());

        }
        // POST: Client/Edit/5
        [HttpPost]
        public ActionResult Update(int Id, ventes vente)
        {
            /*int? qte=vente.qte;
            UpdateQTEVente(Id,qte);*/
             try
                {
                // TODO: Add update logic here
                db.Entry(vente).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Vente");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Delete(int Id)
        {
            return View(db.ventes.Where(x => x.Id == Id).FirstOrDefault());

        }
        [HttpPost]
        public ActionResult Delete(int Id, ventes vente)
        {

            RestoreQTE(Id);
            try
            {
                // TODO: Add delete logic here
                ventes Vdelete = db.ventes.Where(x => x.Id == Id).FirstOrDefault();
                db.ventes.Remove(Vdelete);
                db.SaveChanges();


                return RedirectToAction("Vente");
            }
            catch
            {
                return View();
            }
        }

        public void UpdateQTEVente(int Id,int? qte)
        {
            int? QTE; int? QT; int? QTvente; string num_V;
            var VenteQT = db.ventes.Where(x => x.Id == Id).ToList();
            foreach (var ite in VenteQT)
            {
                QTvente = ite.qte;
                num_V = ite.num_voiture;
                var label = db.voiture.Where(x => x.num_voiture == num_V).ToList();
                foreach (var item in label)
                {
                    QTE = item.stock;
                    QT = QTE + QTvente;
                   QT = QT - qte;
                    UpdateQTE(QT, num_V);
                };
            };
        }
        public void RestoreQTE(int Id)
        {
              int? QTE; int? QT; int? QTvente;string num_V;
                var VenteQT = db.ventes.Where(x => x.Id == Id).ToList();
                foreach (var ite in VenteQT)
                {
                    QTvente = ite.qte;
                    num_V = ite.num_voiture;
                    var label = db.voiture.Where(x => x.num_voiture ==num_V).ToList();
                    foreach (var item in label)
                    {
                        QTE = item.stock;
                        QT = QTE + QTvente;
                        UpdateQTE(QT, num_V);
                    };
                };
        }
        public void UpdateQTE(int? q,string num)
        {
            var voiture = new voiture() { num_voiture = num, stock=q};
            using (var db = new g2vEntities())
            {
                db.voiture.Attach(voiture);
                db.Entry(voiture).Property(x => x.stock).IsModified = true;
                db.SaveChanges();
            }
        }

    }
}